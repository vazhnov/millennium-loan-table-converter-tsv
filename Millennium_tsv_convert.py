#! /usr/bin/env python3

import sys
import re

# SPDX-License-Identifier: MIT

"""
For https://www.bankmillennium.pl/osobiste2/Loans/LoanAccountDetails/LoanDetails
Copypaste the table from the web-browser into .txt file.

It should looks like in `Millennium_tsv_convert_example.txt`.

Then run this script.

See also: https://github.com/svenstaro/mt940-rs
"""


def convert_txt_to_tsv(input_name, output_name):
    """
    Converts text like this into TSV.
    """
    output_data = []
    with open(input_name, encoding='utf-8', mode='r') as file_input:
        read_data = file_input.read().splitlines()
    for line in read_data:
        if line.startswith('Payment'):
            output_data.append(line)
        elif line[:1].isdigit():
            # Remember this string for next iteration
            input_date = line
        elif line.startswith('\t'):
            # No need in TAB between strings, because it is already in "line"
            combined_line = input_date + line
            converted_line = convert_one_line(combined_line)
            output_data.append(converted_line)
        else:
            print('Something is wrong with this line:', line)
    with open(output_name, encoding='utf-8', mode='w') as file_output:
        file_output.write('\n'.join(output_data))


def convert_one_line(old_line):
    """
    Convert date format DD-MM-YYYY into YYYY-MM-DD,
    Convert amounts like 592,507.69 PLN into 592507.69.
    """
    (dd, mm, yyyy, remainder_without_date) = REGEX_TEMPLATE_DATE.findall(old_line)[0]
    new_line = f'{yyyy}-{mm}-{dd}{remainder_without_date}'
    new_line = new_line.replace(' PLN', '')
    new_line = new_line.replace(',', '')
    new_line = new_line.replace(' \t', '\t')
    new_line = new_line.rstrip()
    return new_line


if __name__ == '__main__':
    REGEX_TEMPLATE_DATE = re.compile(r'^(\d+)-(\d+)-(\d+)(\s.+)')     # like '01-12-2051'
    # REGEX_TEMPLATE_AMOUNT = re.compile(r'(\d+,)?(\d+\.\d{2})( PLN)')  # like '2,455.01 PLN' or '562.70 PLN' or '0.00 PLN'
    if len(sys.argv) == 3:
        convert_txt_to_tsv(sys.argv[1], sys.argv[2])
    elif len(sys.argv) == 2:
        convert_txt_to_tsv(sys.argv[1], f'{sys.argv[1]}.tsv')
    else:
        sys.exit('Usage example: Millennium_tsv_convert.py /tmp/Payment_plan_2022-01-21.txt /tmp/Payment_plan_2022-01-21.tsv')
