Creates TSV [tab-separated values](https://en.wikipedia.org/wiki/Tab-separated_values) from `.txt` file, copied from a page with mortgage table.

For https://www.bankmillennium.pl/osobiste2/Loans/LoanAccountDetails/LoanDetails

## How to use

Copypaste the table from the web-browser into `.txt` file.

It should looks like in `Millennium_tsv_convert_example.txt`.

Then run the Python script `Millennium_tsv_convert.py`.

## See also

* https://github.com/svenstaro/mt940-rs — some parser in Rust
